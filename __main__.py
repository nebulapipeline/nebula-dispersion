from Qt.QtWidgets import QApplication, QStyleFactory
from nebula.common import core
from nebula.auth import user
import nebula
import sys

if nebula.DEBUG:
    user.login('admin', 'aa')
else:
    user.login('tactic', 'tactic123')

server = user.get_server()
server.set_project('admin')

from nebula import backends
core.BackendRegistry.register(backends.tactic_backend)
core.BackendRegistry.set('tactic')


from src._ui import Dispersion

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle(QStyleFactory.create('plastique'))
    win = Dispersion()
    win.show()
    sys.exit(app.exec_())