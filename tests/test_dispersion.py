from Qt.QtWidgets import QApplication
from nebula.common import tools
import unittest
from ..src import _ui
import time
import sys
import os

root_path = os.path.dirname(__file__)
files_path = os.path.join(root_path, 'files')

class DispersionTestCase(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.app = QApplication(sys.argv)
        cls.dispersion = _ui.Dispersion()
        cls.dispersion.show()
        QApplication.processEvents()
        time.sleep(1)
        tests_path = os.path.abspath(os.path.dirname(__file__))
        test_files_path = os.path.join(tests_path, 'files')
        file_paths = [os.path.join(test_files_path, phile)
                      for phile in os.listdir(test_files_path)]
        cls.dispersion.populate(file_paths)
        QApplication.processEvents()
        time.sleep(1)
    
    @classmethod
    def tearDownClass(cls):
        for widget in QApplication.topLevelWidgets():
            widget.deleteLater()
    
    def setUp(self):
        QApplication.processEvents()
        time.sleep(1)
        
    def test_construct_job_info(self):
        item = self.dispersion.getItems()[0]
        self.assertEqual(item._constructJobInfo('mrs'), ['model', 'rig',
                                                         'shaded'])
        self.assertEqual(item._constructJobInfo('mr'), ['model', 'rig'])
        self.assertEqual(item._constructJobInfo('ms'), ['model', 'shaded'])
        self.assertEqual(item._constructJobInfo('rs'), ['rig', 'shaded'])
        self.assertEqual(item._constructJobInfo('m'), ['model'])
        self.assertEqual(item._constructJobInfo('r'), ['rig'])
        self.assertEqual(item._constructJobInfo('s'), ['shaded'])
    
    def test_construct_job_info_with_wrong_args(self):
        item = self.dispersion.getItems()[0]
        self.assertEqual(item._constructJobInfo(''), [])
        self.assertRaises(KeyError, item._constructJobInfo, 'wow')
        self.assertRaises(KeyError, item._constructJobInfo, '---')
        
    def test_update(self):
        asset = 'bilal'
        item = self.dispersion.getItemByName(asset)
        self.assertEqual(item.getAsset(), asset)
        self.assertTrue(bool(item._submission_info))
        self.assertEqual(len(item._submission_info.keys()), 1)
    
    def test_populate(self):
        # already populated
        self.assertTrue(len(self.dispersion.getItems()) > 0)
        # try to add a duplicate archive
        num = len(self.dispersion.getItems())
        self.dispersion.populate([os.path.join(files_path, 'new.zip')])
        self.assertEqual(len(self.dispersion.getItems()), num)
    
    
    
    
    
    
    
    
    
    
        
        
        
        
        
        
        
        
        
        
        
        