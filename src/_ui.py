#-------------------------------------------------------------------------------
# This module contains the GUI classes for reading archive files received from
# freelancers as completed tasks and uploading (dispersing) them to the asset
# management system
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
from Qt.QtCompat import loadUi
from Qt.QtWidgets import QMainWindow, QFileDialog, QFrame, QMessageBox
from nebula.common import util, core, tools, gui
import os
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
root_path = util.dirname(__file__, 2)
ui_path = os.path.join(root_path, 'ui')
icon_path = os.path.join(root_path, 'icons')
_backend = core.BackendRegistry.get()
#-------------------------------------------------------------------------------

class Dispersion(QMainWindow):
    __title = 'Dispersion'
    def __init__(self, parent=None):
        super(Dispersion, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'dispersion.ui'), self)
        self.setWindowTitle(self.__title)
        
        # attributes
        self._items = []
        
        # connections
        self.browseButton.clicked.connect(self.browseFiles)
        self.selectAllButton.stateChanged[int].connect(self.selectAll)
        
    def selectAll(self, state):
        for item in self._items:
            item.setSelected(state)
        
    def browseFiles(self):
        '''
        browse filesystem for zip files
        '''
        filenames = QFileDialog.getOpenFileNames(self, self.__title, '',
                                                 '*.zip')
        # handle case for pyside where returned value is a tuple
        if isinstance(filenames, tuple):
            filenames = filenames[0]
        self.populate(filenames)
    
    
    
    def populate(self, archive_files):
        '''
        populate the window with task items
        '''
        if archive_files:
            for archive_file in archive_files:
                archive = tools.AssetTaskArchive(archive_file)
                if archive.hash_info not in self.existing_hashes():
                    item = DisperseItem(self, archive)
                    self.itemLayout.addWidget(item)
                    self._items.append(item)
    
    def getItemByName(self, name):
        for item in self._items:
            if item.getAsset() == name:
                return item
    
    def existing_hashes(self):
        return [item.hash_info() for item in self._items]
    
    def getItems(self):
        return self._items
    
    def showMessage(self, *args, **kwargs):
        return gui.showMessage(self, title=self.__title, **kwargs)
        
            
class DisperseItem(QFrame):
    def __init__(self, parent, task_archive):
        super(DisperseItem, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'disperse_item.ui'), self)
        self._parent = parent
        
        # attributes
        self._task_archive = task_archive
        self._submission_info = {} # context to files mappings
        
        # calls
        self.update()
        
    def setSelected(self, state):
        self.taskInfoBox.setChecked(state)
    
    def isSelected(self):
        return self.taskInfoBox.isChecked()
    
    def _constructJobInfo(self, task_list):
        info = {'s': 'shaded', 'm': 'model', 'r': 'rig'}
        tasks = []
        for task in task_list:
            tasks.append(info[task])
        return tasks
    
    def hash_info(self):
        '''
        return hash_info of the underlying archive file
        '''
        return self._task_archive.hash_info
        
    
    def update(self, task_archive=None):
        '''
        updates the item and ui with task_info and submission info
        '''
        # update with task info
        if task_archive:
            self._task_archive = task_archive
        task_info = self._task_archive.task_info
        self.setAsset(task_info.entity_name)
        self.setCategory(task_info.asset_category)
        self.setProject(task_info.project)
        jobTypes = self._constructJobInfo(task_info.task_list)
        self.setJobType(', '.join(jobTypes))
        
        # update with submission info
        contexts = [context for context in
                    self._task_archive.list_asset_contexts(
                    task_info.entity_name)
                    if context.startswith('submission/')]
        if contexts:
            for context in contexts:
                if '/'.join(context.split('/')[1:]) in jobTypes:
                    self._submission_info[context] = \
                        self._task_archive.list_context_files(
                            task_info.entity_name, context)
            for context, files in self._submission_info.items():
                for phile in files:
                    self.itemLayout.addWidget(
                        SubmissionItem(self, context, phile))
        if self._submission_info:
            # TODO: implement this
            pass
        
    def setAsset(self, name):
        self.assetLabel.setText(name)
    
    def getAsset(self):
        return self.assetLabel.text()
    
    def setCategory(self, category):
        self.categoryLabel.setText(category)
    
    def setProject(self, project):
        self.projectLabel.setText(project)
    
    def setJobType(self, job_type):
        self.jobLabel.setText(job_type)
    
class SubmissionItem(QFrame):
    def __init__(self, parent, context, filename):
        super(SubmissionItem, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'sub_item.ui'), self)
        self._parent = parent
        self.setContext(context)
        self.setFilename(filename)
    
    def setContext(self, context):
        self.contextLabel.setText(context)
    
    def setFilename(self, filename):
        self.filenameLabel.setText(os.path.basename(filename))